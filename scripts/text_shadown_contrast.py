#!/usr/bin/env python

#
# Plugin by 4m1g0
# You can add it by plathing this file in ~/.gimp-X.X/plug-ins and configuring gimp to use that folder in Edit > Preferences > Folders > Plug-ins
#

from gimpfu import *

def text_shadown_contrast(image, drawable, size, blur):
    pdb.gimp_image_undo_group_start(image)
    layer = drawable.copy()
    layer.name = drawable.name + " shadow"
    layer_pos = image.layers.index(drawable)
    image.add_layer(layer, layer_pos+1)
    pdb.gimp_layer_resize_to_image_size(layer)
    pdb.gimp_image_select_item(image, 2, layer)
    pdb.gimp_selection_grow(image, size)
    pdb.gimp_drawable_edit_fill(layer, 0)
    pdb.gimp_selection_none(image)
    pdb.plug_in_gauss(image, layer, blur, blur, 0)
    pdb.gimp_image_undo_group_end(image)
    

register(
    "python-fu-text-shadown-contrast",
    "Blurred shadow arround the alpha channel of the selected layer",
    "Generates a blurred shadow arround the alpha channel of the selected layer",
    "4m1g0", "4m1g0", "2018",
    "Text shadown and contrast",
    "*", 
    [
        (PF_IMAGE, "image", "takes current image", None),
        (PF_DRAWABLE, "drawable", "Input layer", None),
        (PF_SLIDER, "size", "Size", 5, (1, 50, 1)),
        (PF_SLIDER, "blur", "Blur", 5, (1, 50, 1)),
    ],
    [],
    text_shadown_contrast, menu="<Image>/Filters/Light and Shadow")

main()
